import {
  Column,
  Entity,
  BeforeInsert,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Team {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ unique: true })
  username: string;

  @ApiProperty()
  @Column()
  password: string;

  @ApiProperty()
  @Column({default: 'team'})
  role: string;

  @ApiProperty()
  @Column({default: 0})
  hintHelper: number;

  @ApiProperty()
  @Column({default: 0})
  skipHelper: number;

  @ApiProperty()
  @Column({default: 0})
  openHelper: number;

  @ApiProperty()
  @Column({default: 0})
  preventHelper: number;

  @ApiProperty()
  @Column({default: 0})
  passedChallenges: number;

  @ApiProperty()
  @Column()
  @UpdateDateColumn()
  lastTime: Date;

  @ApiProperty()
  @Column()
  @CreateDateColumn()
  preventDate: Date;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }
}
