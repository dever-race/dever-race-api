import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Challenge } from './challenge.entity';
import { Repository, DeepPartial } from 'typeorm';
import { CrudRequest, Override } from '@nestjsx/crud';

@Injectable()
export class ChallengeService extends TypeOrmCrudService<Challenge> {
  constructor(@InjectRepository(Challenge) repo: Repository<Challenge>) {
    super(repo);
  }

  @Override()
  async createOne(
    req: CrudRequest,
    dto: DeepPartial<Challenge>,
  ): Promise<Challenge> {
    const maxChallenge = await this.repo.findOne({ order: { order: 'DESC' } });
    dto.order = maxChallenge ? maxChallenge.order + 1 : 1;

    return super.createOne(req, dto);
  }

  async updateOrder(first: number, second: number): Promise<Challenge[]> {
    const challenges = await this.repo.find({ order: { order: 'ASC' } });

    [challenges[first], challenges[second]] = [
      challenges[second],
      challenges[first],
    ];

    for (let i = 0; i < challenges.length; i++) {
      challenges[i].order = i + 1;
    }

    return this.repo.save(challenges);
  }
}
