import { Controller, UseGuards, Patch, Body, Put, Query } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Challenge } from './challenge.entity';
import { ChallengeService } from './challenge.service';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';

@Crud({
  model: {
    type: Challenge,
  },
})
@Controller('challenges')
@ApiTags('Challenges')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class ChallengeController implements CrudController<Challenge> {
  constructor(public service: ChallengeService) {}

  @Put('/updateOrder')
  async updateOrder(
    @Query('first') first: number,
    @Query('second') second: number,
  ): Promise<Challenge[]> {
    return this.service.updateOrder(first, second);
  }
}
