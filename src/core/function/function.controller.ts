import { Controller, Patch, Query, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { FunctionService } from './function.service';

@Controller('functions')
@ApiTags('Functions')
export class FunctionController {
  constructor(private functionService: FunctionService) {}

  @Patch('reset')
  async reset(): Promise<void> {
    await this.functionService.resetSeason();
    await this.functionService.resetTeam();
  }

  @Patch('active/:seasonId')
  async active(
    @Param('seasonId') seasonId: string,
    @Query('key') key: string,
  ): Promise<boolean> {
    return this.functionService.activeSeason(seasonId, key);
  }

  @Patch('helpers')
  async useHelper(
    @Query('helper') helper: string,
    @Query('destination') destination: string,
    @Query('teamId') teamId: string,
  ): Promise<void> {
    await this.functionService.useHelper(helper, destination, teamId);
  }
}
