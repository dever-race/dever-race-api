import { Column, Entity, BaseEntity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Challenge {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column()
  location: string;

  @ApiProperty()
  @Column('text')
  content: string;

  @ApiProperty()
  @Column('text')
  hint: string;

  @ApiProperty()
  @Column()
  key: string;

  @ApiProperty()
  @Column({ default: 0 })
  order: number;
}
