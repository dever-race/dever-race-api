/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, UseGuards } from '@nestjs/common';
import { SeasonService } from './season.service';
import { Season } from './season.entity';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { Crud, CrudController } from '@nestjsx/crud';

@Crud({
  model: {
    type: Season,
  },
  query: {
    join: {
      challenge: {
        eager: true,
      },
      team: {
        eager: true,
      },
    },
  },
})
@ApiTags('Seasons')
@Controller('seasons')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class SeasonController implements CrudController<Season> {
  constructor(public service: SeasonService) {}
}
