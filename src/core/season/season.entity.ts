import {
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  ManyToOne,
  Column,
  UpdateDateColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Team } from '../team/team.entity';
import { Challenge } from '../challenge/challenge.entity';

@Entity()
@Unique(['team', 'challenge'])
export class Season {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @ManyToOne(type => Team)
  team: Team;

  @ApiProperty()
  @ManyToOne(type => Challenge)
  challenge: Challenge;

  @ApiProperty()
  @Column({default: false})
  showHint: boolean;

  @ApiProperty()
  @Column({default: false})
  passed: boolean;

  @ApiProperty()
  @Column()
  @UpdateDateColumn()
  time?: Date;
}
