import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeamModule } from './core/team/team.module';
import { AuthModule } from './core/auth/auth.module';
import { Team } from './core/team/team.entity';
import { ChallengeModule } from './core/challenge/challenge.module';
import { Challenge } from './core/challenge/challenge.entity';
import { Season } from './core/season/season.entity';
import { SeasonModule } from './core/season/season.module';
import { FunctionController } from './core/function/function.controller';
import { FunctionModule } from './core/function/function.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [Team, Challenge, Season],
      synchronize: true,
    }),
    AuthModule,
    TeamModule,
    ChallengeModule,
    SeasonModule,
    FunctionModule,
  ],
  controllers: [FunctionController],
  providers: [],
})
export class AppModule {}
