import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Season } from '../season/season.entity';
import { Challenge } from '../challenge/challenge.entity';
import { Team } from '../team/team.entity';
import { MoreThan } from 'typeorm';
import * as moment from 'moment';

@Injectable()
export class FunctionService {
  constructor(
    @InjectRepository(Season) private seasonRepo: Repository<Season>,
    @InjectRepository(Challenge) private challengeRepo: Repository<Challenge>,
    @InjectRepository(Team) private teamRepo: Repository<Team>,
  ) {}

  // Open a season
  async openSeason(challenge: Challenge, team: Team): Promise<void> {
    const nextSeason = new Season();
    nextSeason.challenge = challenge;
    nextSeason.team = team;

    await this.seasonRepo.save(nextSeason);
  }

  // Skip a specify season and open next if possible
  async skipSeason(season: Season): Promise<boolean> {
    season.passed = true;
    await this.seasonRepo.save(season);
    await this.updateTeamRace(season.team);

    const seasons = await this.seasonRepo.find({
      relations: ['team', 'challenge'],
      where: { team: { id: season.team.id } },
    });

    if (seasons.filter(x => !x.passed).length === 0) {
      let challenges = await this.challengeRepo.find();
      const seasonIds = seasons.map(x => x.challenge.id);

      challenges = challenges
        .filter(x => !seasonIds.includes(x.id))
        .sort((a, b) => a.order - b.order);

      if (challenges[0]) {
        await this.openSeason(challenges[0], season.team);
      }
    }

    return true;
  }

  // Active season by key and open next if possible
  async activeSeason(seasonId: string, key: string): Promise<boolean> {
    const season = await this.seasonRepo.findOne({
      where: { id: seasonId },
      relations: ['team', 'challenge'],
    });
    if (season.passed) {
      return true;
    }

    if (
      season.challenge.key.trim().toLowerCase() === key.trim().toLowerCase()
    ) {
      return this.skipSeason(season);
    } else {
      season.team.preventDate = moment()
        .add(20, 'seconds')
        .toDate();

      this.teamRepo.save(season.team);
    }

    return false;
  }

  // Update team race
  async updateTeamRace(team: Team): Promise<void> {
    team.passedChallenges++;
    await this.teamRepo.save(team);
  }

  // Reset season state
  async resetSeason(): Promise<void> {
    const challenge = await this.challengeRepo.findOne({
      order: { order: 'ASC' },
    });
    const teams = await this.teamRepo.find();
    const seasons = teams.map(team => {
      const season = new Season();
      season.challenge = challenge;
      season.team = team;

      return season;
    });

    await this.seasonRepo.clear();
    await this.seasonRepo.save(seasons);
  }

  // Reset team state
  async resetTeam(): Promise<void> {
    const teams = (await this.teamRepo.find()).map(team => {
      team.passedChallenges = 0;

      team.lastTime = new Date();
      team.preventDate = new Date();

      team.openHelper = 1;
      team.skipHelper = 1;
      team.hintHelper = 3;
      team.preventHelper = 1;

      return team;
    });

    this.teamRepo.save(teams);
  }

  // Use helper
  async useHelper(
    helper: string,
    destination: string,
    teamId: string,
  ): Promise<void> {
    const team = await this.teamRepo.findOne({ where: { id: teamId } });

    switch (helper) {
      case 'HINT':
        {
          team.hintHelper--;

          const season = await this.seasonRepo.findOne({
            where: { team: { id: teamId }, challenge: { id: destination } },
          });
          season.showHint = true;
          await this.seasonRepo.save(season);
        }

        break;

      case 'OPEN':
        {
          team.openHelper--;

          const challenge = await this.challengeRepo.findOne({
            where: { id: destination },
          });
          await this.openSeason(challenge, team);
        }

        break;

      case 'SKIP':
        {
          team.skipHelper--;

          const season = await this.seasonRepo.findOne({
            where: { team: { id: teamId }, challenge: { id: destination } },
            relations: ['team', 'challenge'],
          });
          await this.skipSeason(season);
        }

        break;

      case 'PREVENT':
        {
          team.preventHelper--;

          const preventedTeam = await this.teamRepo.findOne({
            where: { id: destination },
          });
          const preventDate = moment().add(10, 'minutes');
          preventedTeam.preventDate = preventDate.toDate();

          await this.teamRepo.save(preventedTeam);
        }

        break;

      default:
        break;
    }

    await this.teamRepo.save({
      id: team.id,
      hintHelper: team.hintHelper,
      skipHelper: team.skipHelper,
      openHelper: team.openHelper,
      preventHelper: team.preventHelper,
    });
  }
}
