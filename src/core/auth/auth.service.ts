/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@nestjs/common';
import { TeamService } from '../team/team.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private teamService: TeamService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    if (username === 'admin' && password === 'ADMIN') {
      return {
        name: 'Super Admin',
        role: 'admin'
      };
    }
    
    const user = await this.teamService.findByUsername(username);

    if (user && (await user.comparePassword(password))) {
      return user;
    }
    return null;
  }

  async login(user: any): Promise<any> {
    const payload = {
      id: user.id,
      username: user.username,
      name: user.name,
      role: user.role,
    };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }
}
