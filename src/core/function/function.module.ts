import { Module } from '@nestjs/common';
import { FunctionController } from './function.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Season } from '../season/season.entity';
import { Challenge } from '../challenge/challenge.entity';
import { Team } from '../team/team.entity';
import { FunctionService } from './function.service';

@Module({
  imports: [TypeOrmModule.forFeature([Season, Challenge, Team])],
  controllers: [FunctionController],
  providers: [FunctionService],
  exports: [FunctionService],
})
export class FunctionModule {}
