import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeepPartial } from 'typeorm';
import { Team } from './team.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Override, CrudRequest } from '@nestjsx/crud';
import * as bcrypt from 'bcrypt';

@Injectable()
export class TeamService extends TypeOrmCrudService<Team> {
  constructor(@InjectRepository(Team) repo: Repository<Team>) {
    super(repo);
  }

  async findByUsername(username: string): Promise<Team> {
    return this.repo.findOne({ where: { username } });
  }

  @Override()
  async updateOne(req: CrudRequest, dto: DeepPartial<Team>): Promise<Team> {
    delete dto.password;
    return super.updateOne(req, dto);
  }

  async changePassword(id: string, password: string): Promise<void> {
    const team = await this.repo.findOne(id);

    if (team) {
      password = await bcrypt.hash(password, 10);
      await this.repo.save({ ...team, password });
    }
  }
}
