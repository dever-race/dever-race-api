import { Module } from '@nestjs/common';
import { SeasonController } from './season.controller';
import { SeasonService } from './season.service';
import { Season } from './season.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Challenge } from '../challenge/challenge.entity';
import { Team } from '../team/team.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Season, Challenge, Team])],
  controllers: [SeasonController],
  providers: [SeasonService],
  exports: [SeasonService],
})
export class SeasonModule {}
