import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Season } from './season.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class SeasonService extends TypeOrmCrudService<Season> {
  constructor(@InjectRepository(Season) seasonRepo: Repository<Season>) {
    super(seasonRepo);
  }
}
