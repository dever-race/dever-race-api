/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, UseGuards, Patch, Param, Query } from '@nestjs/common';
import { TeamService } from './team.service';
import { Team } from './team.entity';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { Crud, CrudController } from '@nestjsx/crud';

@Crud({
  model: {
    type: Team,
  },
})
@ApiTags('Teams')
@Controller('teams')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class TeamController implements CrudController<Team> {
  constructor(public service: TeamService) {}

  @Patch(':id/changePassword')
  async changePassword(
    @Param('id') id: string,
    @Query('password') password: string,
  ): Promise<void> {
    await this.service.changePassword(id, password);
  }
}
